# FB_Grow #
ist ein halbautomatisches, überwachtes Gewächshaus.

* Überwachung der Temperatur & ggf. Heizung
* Künstlicher, frei einstellbarer Tag/Nacht-Zyklus mit spezieller Lampe
* Einstellbarer Zyklus / Temperatur
* Display zur Statusanzeuge

Geschrieben in Atmels AVRStudio V7, Programmiert auf einen Arduino Nano V3.

-----

# FB_Grow #
is a halfautomatic, observed greenhouse.

* Monitor Temperatur and heat if neccessary
* Artificial day/night cycles, created with build-in LED-lamp
* Settings for day/night cycle & temperature
* Display for information

Written in Atmel AVR-Studio V7, programmed on an Arduino Nano V3