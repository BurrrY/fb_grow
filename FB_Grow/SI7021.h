
/*
 * SI7021.h
 *
 * Created: 28.04.2016 19:55:25
 *  Author: mail
 */ 


#ifndef SI7021_H_
#define SI7021_H_


#ifndef F_CPU
#define F_CPU 8000000
#endif


#include <stdint-gcc.h>
#include <util/twi.h>

#include "i2cmaster.h"



#define DevSi7021  0x80      // device address of EEPROM 24C02, see datasheet
uint8_t SI7021_readTemp();
uint8_t SI7021_readHumi();
uint16_t measureSI7021(uint8_t command);
#endif /* SI7021_H_ */