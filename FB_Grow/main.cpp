/*
 * FB_Grow.cpp
 *
 * Created: 23.04.2016 13:37:11
 * Author : mail
 */
 #ifndef F_CPU
 #define F_CPU 8000000
 #endif
 
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>
#include <avr/eeprom.h>
#include <stdio.h> //for sprintf

volatile uint16_t milliseconds;
volatile uint8_t seconds;
volatile uint8_t minutes;
volatile uint8_t hours;
volatile uint8_t days;
volatile uint8_t tick;

#include "i2cmaster.h"

#include "LCD.h"
#include "SI7021.h"

#define ON 1
#define OFF 0
#define TRUE 1
#define FALSE 0


//Light definition 
#define LIGHT_PIN PINC1
#define LIGHT_PORT PORTC
#define LIGHT_PORT_DDR DDRC
uint8_t OnOffCycleH = 12;
uint8_t eeOnOffCycleH EEMEM = 12;
uint8_t LightOnDurationH = 8;
uint8_t eeLightOnDurationH EEMEM = 8;
uint8_t lightOnState;

//Temperature Definition
uint8_t TEMP_THRES_LOW = 21;
uint8_t eeTEMP_THRES_LOW EEMEM = 21;
uint8_t TEMP_THRES_HIGH = 30;
uint8_t eeTEMP_THRES_HIGH EEMEM = 30;
uint8_t latestTemp;
uint8_t latestHumi;



//Heater Definition //A1
uint8_t heaterOn = FALSE;
#define HEAT_PIN PINC0
#define HEAT_PORT PORTC
#define HEAT_PORT_DDR DDRC

//Fan Definition
uint8_t fanOn = FALSE;
#define FAN_PIN PINC2
#define FAN_PORT PORTC
#define FAN_PORT_DDR DDRC

//Buttons
#define BUTTON_DDR DDRD
#define BUTTON_PORT PORTD
#define BUTTON_PINS PIND

#define BUTTON_UP PIND3
#define BUTTON_DOWN PIND2
#define BUTTON_SELECT PIND4

//LCD Row1 & 2
char R1[16];
char R2[16];
#define DISP_DURATION 20
uint8_t crnt_DISP_DURATION = 0;


void setLight(uint8_t state);
void readSens();
void setHeater(uint8_t s);
void setFan(uint8_t state);
void settings();

enum displayState {
	TIME, 
	SENS,
	STATE	
};
displayState crntOutput = TIME;

enum settingsPage {
	NONE,
	CYCLE,
	LIGHT,
	TEMP_MIN,
	TEMP_MAX,
	CRNT_HOURS
};
settingsPage currentSettingsPage = NONE;

void eepInit()
{	
	OnOffCycleH = eeprom_read_byte (&eeOnOffCycleH);
	if(OnOffCycleH == 0xFF)
	{
		OnOffCycleH=12;
		eeprom_update_byte(&eeOnOffCycleH, OnOffCycleH);
	}
		
	LightOnDurationH = eeprom_read_byte (&eeLightOnDurationH);
	if(LightOnDurationH == 0xFF)
	{
		LightOnDurationH=8;
		eeprom_update_byte(&eeLightOnDurationH, LightOnDurationH);
	}
	
	TEMP_THRES_HIGH = eeprom_read_byte (&eeTEMP_THRES_HIGH);
	if(TEMP_THRES_HIGH == 0xFF)
	{
		TEMP_THRES_HIGH=35;
		eeprom_update_byte(&eeTEMP_THRES_HIGH, TEMP_THRES_HIGH);
	}
	
	TEMP_THRES_LOW = eeprom_read_byte (&eeTEMP_THRES_LOW);
	if(TEMP_THRES_LOW == 0xFF)
	{
		TEMP_THRES_LOW=20;
		eeprom_update_byte(&eeTEMP_THRES_LOW, TEMP_THRES_LOW);
	}
}

int main(void)
{
	DDRB |= (1 << DDB5);
	TIMSK0 = (1<<OCIE0A); //Interrupt erlauben
	
	//Setup Timer
	//TCCR0A = (1<<COM0A0); //Toggle OC0A on Compare Match
	TCCR0A = (1<<WGM01); //CTC-Mode
	TCCR0B = ((1<<CS01) | (1<<CS00)); //clk I/O /64 (From prescaler)
	
	OCR0A = 250-1;
	
	sei();
	
	//Init	
	//Light
	LIGHT_PORT_DDR |= (1 << LIGHT_PIN);
	lightOnState = ON;
	setLight(lightOnState);
	
	//Heat
	HEAT_PORT_DDR |= (1 << HEAT_PIN);	
	setHeater(OFF);
	setFan(OFF);
	
	//BUTTON PullUp
	BUTTON_DDR &= ~((1 << BUTTON_DOWN)|(1 << BUTTON_UP)|(1 << BUTTON_SELECT));
	BUTTON_PORT |= ((1 << BUTTON_DOWN)|(1 << BUTTON_UP)|(1 << BUTTON_SELECT));
	
	//LCD
	lcd_init();
	
	_delay_ms(100);	
	lcd_string("FB_Grow v1.0");
	
	//SETTINGS FROM EEPROM
	eepInit();
	
	//I2C
	i2c_init();
	
    /* Replace with your application code */
    while (1) 
    {
		readSens();
		
		if(crntOutput == TIME)
		{
			lcd_setcursor(0,1);
			lcd_string("FB_Grow v1.0");
			sprintf(R1, "%02d:%02d:%02d |%3d|%2d", hours, minutes, seconds, OnOffCycleH, LightOnDurationH);
			lcd_setcursor(0,2);
			lcd_string(R1);
			
			if(crnt_DISP_DURATION++ > DISP_DURATION) {
				crntOutput = SENS;
				crnt_DISP_DURATION = 0;
				lcd_clear();
			}
		}
		else if(crntOutput == SENS)
		{
			lcd_setcursor(0,1);
			if(heaterOn)
				sprintf(R1, "Temp: %02dC ON", latestTemp);
			else
				sprintf(R1, "Temp: %02dC OFF", latestTemp);
				
			lcd_string(R1);
			
			
			lcd_setcursor(0,2);
			if(fanOn)
				sprintf(R2, "Humi: %02d%% ON", latestTemp);
			else
				sprintf(R2, "Humi: %02d%% OFF", latestHumi);			
			lcd_string(R2);
			
			if(crnt_DISP_DURATION++ > DISP_DURATION) {
				crntOutput = TIME;
				crnt_DISP_DURATION = 0;
				lcd_clear();
			}
		}
		

			if ( !(BUTTON_PINS & (1<<BUTTON_SELECT)) ) {
				settings();				
				_delay_ms(50);
			}
			_delay_ms(500);
		}
    
}

void settings() {
	_delay_ms(500);
	if(currentSettingsPage == NONE)
	{
		currentSettingsPage = CYCLE;
	}
	
	
	if(currentSettingsPage == CYCLE)
	{
		lcd_clear();
		_delay_ms(50);
		lcd_string("Day/Night-Dur.:");
		
		lcd_setcursor(0,2);
		sprintf(R2, "%02d hours", OnOffCycleH);
		lcd_string(R2);
		
		while(currentSettingsPage == CYCLE) 
		{
			//NEXT PAGE
			if ( !(BUTTON_PINS & (1<<BUTTON_SELECT)) ) 
			{
				currentSettingsPage = LIGHT;
				eeprom_update_byte(&eeOnOffCycleH, OnOffCycleH);
				_delay_ms(400);
			}
				
				
			if ( !(BUTTON_PINS & (1<<BUTTON_UP))) 
			{	
				if(OnOffCycleH < 24) 
				{			
					lcd_setcursor(0,2);
					sprintf(R2, "%02d hours", ++OnOffCycleH);
					lcd_string(R2);
				}
				_delay_ms(200);
				
			}
			
			if ( !(BUTTON_PINS & (1<<BUTTON_DOWN))) 
			{
				if(OnOffCycleH > 2 && OnOffCycleH > LightOnDurationH) 
				{
					lcd_setcursor(0,2);
					sprintf(R2, "%02d hours", --OnOffCycleH);
					lcd_string(R2);
				}
				_delay_ms(200);
			}
			_delay_ms(200);
			
		}
	}

	if(currentSettingsPage == LIGHT)
	{
		lcd_clear();
		_delay_ms(50);
		lcd_string("Light on:");
	
		lcd_setcursor(0,2);
		sprintf(R2, "%02d hours", LightOnDurationH);
		lcd_string(R2);
	
		while(currentSettingsPage == LIGHT) 
		{
			//NEXT PAGE
			if ( !(BUTTON_PINS & (1<<BUTTON_SELECT)) ) 
			{
				currentSettingsPage = TEMP_MIN;
				eeprom_update_byte(&eeLightOnDurationH, LightOnDurationH);
				_delay_ms(400);
			}
		
		
			if ( !(BUTTON_PINS & (1<<BUTTON_UP))) 
			{
				if(LightOnDurationH < 24 && LightOnDurationH < OnOffCycleH) 
				{
					lcd_setcursor(0,2);
					sprintf(R2, "%02d hours", ++LightOnDurationH);
					lcd_string(R2);
				}
				_delay_ms(200);
			
			}
		
			if ( !(BUTTON_PINS & (1<<BUTTON_DOWN))) 
			{
				if(LightOnDurationH > 1) {
					lcd_setcursor(0,2);
					sprintf(R2, "%02d hours", --LightOnDurationH);
					lcd_string(R2);
				}
				_delay_ms(200);
			}
			_delay_ms(200);
		
		}
	}
	
	
	if(currentSettingsPage == TEMP_MIN)
	{
		lcd_clear();
		_delay_ms(50);
		lcd_string("Temp Min:");
		
		lcd_setcursor(0,2);
		sprintf(R2, "%03d C", TEMP_THRES_LOW);
		lcd_string(R2);
		
		while(currentSettingsPage == TEMP_MIN)
		{
			//NEXT PAGE
			if ( !(BUTTON_PINS & (1<<BUTTON_SELECT)) )
			{
				currentSettingsPage = TEMP_MAX;
				eeprom_update_byte(&eeTEMP_THRES_LOW, TEMP_THRES_LOW);
				_delay_ms(400);
			}
			
			
			if ( !(BUTTON_PINS & (1<<BUTTON_UP)))
			{
				if(TEMP_THRES_LOW < 100 && TEMP_THRES_LOW < TEMP_THRES_HIGH-5)
				{
					lcd_setcursor(0,2);
					sprintf(R2, "%03d C", ++TEMP_THRES_LOW);
					lcd_string(R2);
				}
				_delay_ms(200);
				
			}
			
			if ( !(BUTTON_PINS & (1<<BUTTON_DOWN)))
			{
				if(TEMP_THRES_LOW > 1) {
					lcd_setcursor(0,2);
					sprintf(R2, "%03d C", --TEMP_THRES_LOW);
					lcd_string(R2);
				}
				_delay_ms(200);
			}
			_delay_ms(200);
			
		}
	}
	
	
	
	if(currentSettingsPage == TEMP_MAX)
	{
		lcd_clear();
		_delay_ms(50);
		lcd_string("Temp Max:");
		
		lcd_setcursor(0,2);
		sprintf(R2, "%03d C", TEMP_THRES_HIGH);
		lcd_string(R2);
		
		while(currentSettingsPage == TEMP_MAX)
		{
			//NEXT PAGE
			if ( !(BUTTON_PINS & (1<<BUTTON_SELECT)) )
			{
				currentSettingsPage = CRNT_HOURS;
				eeprom_update_byte(&eeTEMP_THRES_HIGH, TEMP_THRES_HIGH);
				_delay_ms(400);
			}
			
			
			if ( !(BUTTON_PINS & (1<<BUTTON_UP)))
			{
				if(TEMP_THRES_HIGH < 100)
				{
					lcd_setcursor(0,2);
					sprintf(R2, "%03d C", ++TEMP_THRES_HIGH);
					lcd_string(R2);
				}
				_delay_ms(200);
				
			}
			
			if ( !(BUTTON_PINS & (1<<BUTTON_DOWN)))
			{
				if(TEMP_THRES_HIGH > 1  && TEMP_THRES_HIGH > TEMP_THRES_LOW+5) {
					lcd_setcursor(0,2);
					sprintf(R2, "%03d C", --TEMP_THRES_HIGH);
					lcd_string(R2);
				}
				_delay_ms(200);
			}
			_delay_ms(200);
			
		}
	}
	
	
	
	if(currentSettingsPage == CRNT_HOURS)
	{
		lcd_clear();
		_delay_ms(50);
		lcd_string("CurrentCyclePos:");
		
		lcd_setcursor(0,2);
		sprintf(R2, "%02d hours in", hours);
		lcd_string(R2);
		
		while(currentSettingsPage == CRNT_HOURS)
		{
			//NEXT PAGE
			if ( !(BUTTON_PINS & (1<<BUTTON_SELECT)) )
			{
				currentSettingsPage = NONE;
				
				//Do Check here because normally its done every FULL hour
				if(hours >= LightOnDurationH)
					setLight(OFF);
				
				_delay_ms(400);
			}
			
			
			if ( !(BUTTON_PINS & (1<<BUTTON_UP)))
			{
				if(hours < OnOffCycleH-1)
				{
					lcd_setcursor(0,2);
					sprintf(R2, "%02d hours in", ++hours);
					lcd_string(R2);
				}
				_delay_ms(200);
				
			}
			
			if ( !(BUTTON_PINS & (1<<BUTTON_DOWN)))
			{
				if(hours > 1) {
					lcd_setcursor(0,2);
					sprintf(R2, "%02d hours in", --hours);
					lcd_string(R2);
				}
				_delay_ms(200);
			}
			_delay_ms(200);
			
		}
	}
}

ISR(TIMER0_COMPA_vect)
{
	milliseconds++;
	if(milliseconds == 1000)
	{
		seconds++;
		milliseconds = 0;
		
		//Toggle blue LED
		PORTB ^= ( 1 << PINB5 );
		
		if(seconds == 60)
		{
			//Happens every minute
			minutes++;
			seconds = 0;
			
			if(minutes == 60)
			{
				//Happens every Hour
				hours++;
				minutes = 0;
				
				if(hours == LightOnDurationH)
				{
					setLight(OFF);
				}
				
				if(hours == OnOffCycleH)
				{
					setLight(ON);
					hours = 0;
				}
			}
		}
	}
}



void readSens()
{
	latestTemp = SI7021_readTemp();
	latestHumi = SI7021_readHumi();
	if(latestTemp < TEMP_THRES_LOW)
	{
		setHeater(ON);
		setFan(OFF);
}
else if(latestTemp > TEMP_THRES_LOW + 2)
{
	setHeater(OFF);
	setFan(OFF);
}
	else if(latestTemp > TEMP_THRES_HIGH)
	{
		setHeater(OFF);
		setFan(ON);
	}
}

void setLight(uint8_t state) {
	if(state == OFF) {
		LIGHT_PORT |= (1<<LIGHT_PIN);
	} else {
		LIGHT_PORT &= ~(1<<LIGHT_PIN);
	}
}

void setHeater(uint8_t s)
{
	heaterOn = s;
	if(s == OFF) {
		HEAT_PORT |= (1<<HEAT_PIN);
	} else {
		HEAT_PORT &= ~(1<<HEAT_PIN);
	}
}

void setFan(uint8_t state)
{
	fanOn = state;

}