/*
 * SI7021.cpp
 *
 * Created: 28.04.2016 19:55:39
 *  Author: mail
 */ 


#include "SI7021.h"
#include <util\delay.h>
#include "i2cmaster.h"

uint8_t SI7021_readTemp() {
	
	uint16_t Temp_Code = measureSI7021(0xE3);
	float UT = (float)Temp_Code;
	uint8_t temp = (uint8_t)(((UT*175.72f)/65536 - 46.85f));
	return temp;
}


uint8_t SI7021_readHumi() {
	
	uint16_t RH_Code = measureSI7021(0xE5);
	float UT = (float)RH_Code;
	uint8_t rh = (uint8_t)(((UT*(float)125)/(float)65536 - (float)6));
	return rh;
}

uint16_t measureSI7021(uint8_t command) {
	uint16_t result = 0;
	i2c_init();
	uint8_t ret = i2c_start(DevSi7021+I2C_WRITE); //i2c_start(0)
	if ( ret ) {
		// failed to issue start condition, possibly no device found 
		i2c_stop();
		return 99;
	}
	
	
	//return 88;
	
						//FOUND_MODULE = i2c.address(0, ADDR, i2c.TRANSMITTER)
	i2c_write(command); //i2c.write(0, commands)
	i2c_stop();			//i2c.stop(0)
	
	i2c_start_wait(DevSi7021+I2C_READ);	//i2c.start(0)
										//i2c.address(0, ADDR,i2c.RECEIVER)
	_delay_ms(2);
	result = (i2c_readAck() << 8); //c = i2c.read(0, length)
	result |= i2c_readNak();
	
	i2c_stop();			//i2c.stop(0)

	return result;
}	